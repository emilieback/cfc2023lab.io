// Modified version of micromorph's spa router
// https://github.com/natemoo-re/micromorph/blob/main/src/spa.ts

import { isElement, normalizeRelativeURLs } from "./utils";
import micromorph from "micromorph";

const isLocalUrl = (href: string) => {
  try {
    const url = new URL(href);
    if (window.location.origin === url.origin) {
      if (url.pathname === window.location.pathname) {
        return !url.hash;
      }
      return true;
    }
  } catch (e) {}
  return false;
};

const getUrl = ({ target }: Event, opts: Options): URL | undefined => {
  if (!isElement(target)) return;
  const a = target.closest("a");
  if (!a) return;
  if (typeof opts.include === "string" && !a.matches(opts.include)) return;
  if (typeof opts.include === "function" && !opts.include(a)) return;
  if ("routerIgnore" in a.dataset) return;
  const { href } = a;
  if (!isLocalUrl(href)) return;
  return new URL(href);
};

let noop = () => {};
let p: DOMParser;

async function navigate(url: URL, isBack: boolean = false, opts: Options) {
  const { beforeDiff = noop, afterDiff = noop, beforeNavigate = noop } = opts;
  p = p || new DOMParser();
  beforeNavigate();

  const contents = await fetch(`${url}`)
    .then((res) => res.text())
    .catch(() => {
      window.location.assign(url);
    });

  if (!contents) return;

  if (!isBack) {
    history.pushState({}, "", url);
    // undefined defaults to true
    if (opts.scrollToTop ?? true) {
      window.scrollTo({ top: 0 });
    }
  }

  const html = p.parseFromString(contents, "text/html");
  normalizeRelativeURLs(html, url);
  beforeDiff(html);
  const title = html.querySelector("title");

  if (title) {
    document.title = title.text;
  }

  // await micromorph(document, html);

  const scrollPositionsMap: Record<string, { left: number; top: number }> = {};
  let keepScrollElements = document.querySelectorAll(
    "[data-router-keep-scroll]"
  );
  keepScrollElements.forEach((e) => {
    const id = (e as HTMLElement).dataset.routerKeepScroll;
    scrollPositionsMap[id] = { left: e.scrollLeft, top: e.scrollTop };
  });

  await micromorph(document.head, html.head);
  const elementsToReplaceFrom = document.querySelectorAll(
    "[data-router-replace]"
  );

  let didPartialReplace = false;

  for (let i = elementsToReplaceFrom.length - 1; i >= 0; i--) {
    const routerReplaceId = (elementsToReplaceFrom[i] as HTMLElement).dataset
      .routerReplace;

    const elementTo = html.querySelector(
      `[data-router-replace="${routerReplaceId}"]`
    );

    if (elementTo === null) continue;
    elementsToReplaceFrom[i].replaceWith(elementTo);
    didPartialReplace = true;
    break;
  }

  if (!didPartialReplace) document.body.replaceWith(html.body);

  keepScrollElements = document.querySelectorAll("[data-router-keep-scroll]");
  keepScrollElements.forEach((e) => {
    const id = (e as HTMLElement).dataset.routerKeepScroll;
    const scrollOptions = scrollPositionsMap[id];
    if (scrollOptions === undefined) return;
    e.scroll(scrollOptions);
  });

  afterDiff();
}

interface Options {
  beforeNavigate?: () => void | Promise<void>;
  beforeDiff?: (newDocument: Document) => void | Promise<void>;
  afterDiff?: () => void | Promise<void>;
  include?: string | ((element: HTMLAnchorElement) => boolean);
  scrollToTop?: boolean;
}

export default function listen(opts: Options = {}) {
  if (typeof window !== "undefined") {
    window.addEventListener("click", async (event) => {
      const url = getUrl(event, opts);
      if (!url) return;
      event.preventDefault();

      try {
        navigate(url, false, opts);
      } catch (e) {
        window.location.assign(url);
      }
    });

    window.addEventListener("popstate", () => {
      if (window.location.hash) return;
      try {
        navigate(new URL(window.location.toString()), true, opts);
      } catch (e) {
        window.location.reload();
      }
      return;
    });
  }
}
