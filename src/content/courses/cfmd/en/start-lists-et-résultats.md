---
title: Start lists and results
order: 2
draft: false
---

**Start lists:**
[﻿per category](/images/uploads/heures-de-départ-par-catégorie-_cfmdetopen.pdf) / [per club](/images/uploads/heures-de-départ-par-club-_cfmdetopen.pdf)

**Live results:** [﻿CFMD et open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=5)

**Official results:**

**A﻿nalyse :** [Livelox](https://www.livelox.com/Events/Show/96103/Championnat-de-France-Moyenne-Distance-2023-open-)
