---
title: Informations about Middle Distance French Championships
order: 3
draft: false
---
20/05/2022
P﻿lace: Plateau de Retord **(new map)**

World Ranking Event (W21 and M21)

<p style="display: grid; grid-template-columns: auto auto auto; justify-items: center;align-items: center; gap: 1rem;"><a style="height: 100%;" href="/images/uploads/bulletin2_en_v1.pdf"><img style="height: 100%;" src="/images/uploads/txt_bulletinwre2.svg" alt="BulletinWRE"></a><a style="height: 100%;" href="/images/uploads/bulletin-1_en_v3.pdf"><img style="height: 100%;" src="/images/uploads/txt_bulletinwre1.svg" alt="BulletinWRE"></a><a style="height: 100%;" href="/images/uploads/230520_annonce-de-course_cfmd_v01.pdf"> <img style="height: 100%;" src="/images/uploads/txt_annoncecourse-en.svg" alt="annonce de course"></a></p>
