---
title: Day program
order: 1
draft: false
---
08:30 Reception opening

9:45 - 13:30 : First start (competitors: French championship and WRE)

14:00 First start for leisure participants

13:30 - 17:00 Starts for "open" competitors (not qualified for CFMD)

14:00 Prize-giving ceremony (for CFMD and WRE)

Competition stops 1h30 after the last start (controls are picked out).