---
title: Animation JO !
order: 4
draft: false
---
<img src="/images/uploads/logo_jo_gris_long.svg" alt="logo_JO" style="width:85%;margin:auto;display:block;">



Le comité d'organisation du CFC 2023 a le plaisir d'inviter nos plus jeunes orienteurs à participer aux premiers JO du CFC ! 🏆
Un événement ouvert aux catégories HD8 / HD10 / HD12 et qui se déroulera en continu sur la journée des championnats de France moyenne distance.

Au programme : 

**5 ateliers** : parcours d'agilité 🤸biathlon 🔫, vrai-faux-manquant, lab'O 🔬, circuit multipostes 🤯.

**Un but :** collecter le moins de points possible. 🥈🥇🥉
Pour chaque épreuve le jeune marquera le nombre de points lié à sa place sur l'atelier.
Exemple : Timéo finit 5ème de l’atelier A il marque 5 points et ainsi de suite...
Il y a 3 catégories : HD 8 / HD 10 / HD 12

Chaque jeune a la journée pour faire l'ensemble des jeux ; il pourra réaliser sa course avant, pendant ou même après les jeux.
L'inscription aux JO est réalisée automatiquement pour les inscrits du samedi, et est également possible en amont par mail et restera ouverte sur place bien sûr !
Il y aura un classement par catégorie et un podium club (addition des trois meilleurs scores du club).
Les récompenses seront remises dimanche, en ouverture des cérémonies du CFC et sauront régaler les plus gourmands ! 🍫