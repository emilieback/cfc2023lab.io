---
title: Inscriptions
order: 5
draft: false
---
* Pour les coureurs licenciés en France et les compétiteurs étrangers : i﻿nscriptions sur le site de la FFCO.
* Participants l﻿oisir : toutes les informations sur la page [Loisir](https://cfc2023.fr/fr/Loisir/).
* World Ranking Event (D/H21 Elite) : [Eventor](https://eventor.orienteering.org/Events/Show/7651).

*A noter que compétiteurs souhaitant participer à la course WRE doivent s'inscrire sur les 2 sites : FFCO et Eventor. (voir les bulletins WRE pour plus d'informations).*