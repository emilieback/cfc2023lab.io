---
title: Listes de départ et résultats
order: 2
draft: false
---
**H﻿oraires de départ :** (﻿CFMD et open)
[﻿par catégorie](/images/uploads/heures-de-départ-par-catégorie-_cfmdetopen.pdf) / [par club](/images/uploads/heures-de-départ-par-club-_cfmdetopen.pdf)

**Résultats live :** [﻿CFMD et open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=5)

**R﻿ésultats officiels :**

**A﻿nalyse :** [Livelox](https://www.livelox.com/Events/Show/96103/Championnat-de-France-Moyenne-Distance-2023-open-)