---
title: Informations sur la Régionale Moyenne Distance
order: 1
draft: false
---

19/05/2022 - fin de matinée au Plateau de Retord

C﻿ourse comptant pour le Classement National.

<a href="/images/uploads/23.05.19_annonce-de-course_regionale_v01.pdf" > <img src="/images/uploads/txt_annoncecourse.svg" alt="annonce de course" style="width: 30%; margin: auto;"></a>
