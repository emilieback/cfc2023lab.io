---
title: Inscriptions
order: 6
draft: false
---
I﻿nscriptions sur le [site de la FFCO](https://licences.ffcorientation.fr/inscriptions/2581/)

**B﻿ourses aux équipiers**

V﻿otre équipe de relais est incomplète ? Nous mettons à votre disposition un fichier pour vous permettre de trouver des coéquipiers !

*Note : il ne s'agit pas d'un fichier d'inscription mais seulement d'un document vous permettant de vous mettre en contact entre vous.*

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a href="https://docs.google.com/spreadsheets/d/1CcWzRGHlgZsQl5eg1GuRTvI0LZprjrdjk1vTcq94gHQ/edit?usp=sharing" style="display:contents;"> <img src="/images/uploads/txt_bourseequ.svg" alt="Bourse aux équipiers" style="height: 80%;"></a>
</p>