---
title: Programme du jour
order: 1
draft: false
---
05:30 : ouverture de l’accueil

06:30 : départ N1

06:40 : départ N2

07:00 : départ N3

07:30 : départ N4

08:00 : départ TTG

08:20 : départ Relais Retord

12:30 : remise des récompenses

1﻿4:30 : fermeture des circuits et débalisage

*Conformément au règlement, l’horaire de départ en masse des retardataires sera défini sur décision de l’arbitre et annoncé au micro.*