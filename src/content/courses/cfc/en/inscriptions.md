---
title: Registration
order: 6
draft: false
---
See [F﻿FCO website](https://licences.ffcorientation.fr/inscriptions/2581/)

Do you need a team? 

If you do not have a whole team but want to run, you can use this file to find a team. \
N﻿ote: this is for help, it is not an official registration!

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a href="https://docs.google.com/spreadsheets/d/1CcWzRGHlgZsQl5eg1GuRTvI0LZprjrdjk1vTcq94gHQ/edit?usp=sharing" style="display:contents;"> <img src="/images/uploads/txt_bourseequ_en.svg" alt="Bourse aux équipiers" style="height: 80%;"></a>
</p>