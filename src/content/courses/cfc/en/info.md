---
title: Informations about CFC
order: 1
draft: false
---
T﻿he list of the qualified clubs is available on the [FFCO website](https://www.ffcorientation.fr/courses/qualifications/).

**If﻿ you do not have a team to run at CFC (not licensed in a french club), you can participate in "Relais Retord"**, which consists of 3 legs (course are 4.5km long and technical level is purple (hard) and is opened to anyone.

<p style="display: grid;grid-template-columns: 1fr 1fr;justify-items: center;align-items: center;">
<a href="/images/uploads/230521_annonce-de-course_cfc_v01.pdf" style="display:contents;"> <img src="/images/uploads/txt_annoncecourse-en.svg" alt="annonce de course" style="height: 80%;"></a>
</p>
