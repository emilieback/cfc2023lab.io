---
title: Start lists and results
order: 3
draft: false
---
**Start lists:** 

* [Startlist C﻿FRS](/images/uploads/equipes_cfrs.pdf).  S﻿print relay starts at 14:00.
* [Sprint open, per course](/images/uploads/heures-de-départ-par-circuits-_sprintopen.pdf) / [Sprint open, per club](/images/uploads/heures-de-départ-par-club-_sprintopen.pdf)

**Live results:** [CFRS](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=1) / [Sprint open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=3)

**Official results:**

**Analysis:** [Livelox CFRS](https://www.livelox.com/Events/Show/96072/Championnat-de-France-Relais-Sprint-2023) /﻿ [Livelox sprint open](https://www.livelox.com/Events/Show/96084/Sprint-Open-CFRS-)