---
title: Listes de départ et résultats
order: 3
draft: false
---
**Listes et horaires de départ :**

* [Equipes C﻿FRS](/images/uploads/equipes_cfrs.pdf)  : Départ en masse à 14:00.
* [Sprint open, par circuit](/images/uploads/heures-de-départ-par-circuits-_sprintopen.pdf) / [Sprint open, par club](/images/uploads/heures-de-départ-par-club-_sprintopen.pdf)

**Résultats live :** [CFRS](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=1) / [Sprint open](http://co-live.fr/olive/resultats.php?Action=ChoixCat&IdCourse=3)

**R﻿ésultats officiels :**

**A﻿nalyse :** [Livelox CFRS](https://www.livelox.com/Events/Show/96072/Championnat-de-France-Relais-Sprint-2023) /﻿ [Livelox sprint open](https://www.livelox.com/Events/Show/96084/Sprint-Open-CFRS-)