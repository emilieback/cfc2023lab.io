---
title: Terrain et tracés
order: 4
draft: false
---
N﻿ouvelle carte

### Le mot du traceur, Cyril Soucat :

En traçant le CFRS de cette année, j’ai tenté d’associer à la fois sécurité, spectacle et plaisir d’orientation ! Vous naviguerez en cherchant les bons choix sur un terrain très roulant, principalement composé d’asphalte. Entre immeubles 🏨🏢 assez similaires les uns aux autres, ouvertures à chercher dans les clôtures et vigilances à l’emplacement exact du poste 🧐, les coureurs devront de toute évidence trouver un compromis entre vitesse et précision ! 🧘🏃🏻‍♀️
Ça reste très subjectif, mais en tant qu'orienteur, c'est des parcours sur lesquels j'aurai aimé courir ! 😉
Coté spectacle, venez nombreux 🏟️📢 ! Vous pourrez encourager les coureurs au milieu du parcours sur un long couloir spectacle et lors de leur arrivée ! L’ambiance sera forcément au rendez-vous ! 🎉📢🎤