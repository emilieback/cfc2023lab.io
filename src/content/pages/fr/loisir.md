---
title: Loisir
translatedSlug: Loisir
summary: Page destiné aux participants Loisir
draft: false
---
# L﻿oisir

### C﻿ourses destinées au public loisir

Vous êtes non licencié en club et vous souhaitez découvrir l’univers de la course d’orientation ? 

**Participez librement et à votre rythme** avec le même matériel que les compétiteurs. P﻿lusieurs circuits seront proposés sur chacune des étapes, à choisir en fonction de vos envies et de votre niveau !

Pour vous inscrire :

* **Circuits urbains à Oyonnax – Bellignat / jeudi 18 mai 2023** / départs entre 16h30 et 18h00 : [cliquez ici pour vous inscrire !](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/inscriptions-circuits-loisirs-championnat-de-france-course-d-orientation-2023)
* **Circuits en milieu naturel au Plateau de Retord / vendredi 19 mai 2023** / départs entre 12h30 et 14h30 : [cliquez ici pour vous inscrire !](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/circuits-en-milieu-naturel-au-plateau-de-retord-vendredi-19-mai-2023-2)﻿
* **Circuits en milieu naturel au Plateau de Retord / samedi 20 mai 2023** / départs entre 13h00 et 15h00 : [cliquez ici pour vous inscrire !](https://www.helloasso.com/associations/comite-departemental-de-course-d-orientation-de-l-ain/boutiques/circuits-en-milieu-naturel-au-plateau-de-retord-samedi-20-mai-2023-2)

Le coût d’inscription comprend une licence journalière (« Pass’orientation ») ​obligatoire avec assurance nécessaire pour pouvoir participer aux manifestations organisées par la Fédération Française de Course d’Orientation (FFCO).