---
title: Hébergements
translatedSlug: hebergements
summary: Hébergements pour le WE du CFC 2023.
draft: false
---

# Hébergements pour le WE du CFC 2023

Note : l'accès à l'aréna du Plateau de Retord (CFC, CFMD, MD) se fera par le col de Cuvéry, et non par les Plans d'Hotonnes.

Aréna unique du 19 au 21 mai et possibilité pour les camping-cars de rester sur le parking la nuit. Des toilettes sèches seront disponibles sur ce parking.

![Carte d'accès aux 2 arénas - Rhône-Alpes](/images/uploads/carte-ra.svg "Carte d'accès - Rhône-Alpes")

Retrouvez plus d'informations pour des hébergements ci-dessous (offices de tourisme).
