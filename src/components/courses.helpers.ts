import type { CourseConfig } from "@src/content/courses/_course-config.model";

export function getSortedCourses(courses: CourseConfig[], locale: "fr" | "en") {
  return courses
    .sort((firstCourse, secondCourse) => {
      const firstDate = new Date(firstCourse.date);
      const secondDate = new Date(secondCourse.date);

      return firstDate.valueOf() - secondDate.valueOf();
    })
    .map((course) => {
      const date = new Date(course.date);
      return {
        ...course,
        prettyDate: date.toLocaleDateString("fr"),
        url: `/${locale}/courses/${
          locale === "fr" ? course.frenchSlug : course.englishSlug
        }`,
      };
    });
}
