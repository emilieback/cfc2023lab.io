import { defineConfig } from "astro/config";
import path from "path";
import { fileURLToPath } from "url";
import svelte from "@astrojs/svelte";
import compress from "astro-compress";
import image from "@astrojs/image";
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

export default defineConfig({
  sitemap: true,
  site: "https://cfc2023.gitlab.io",
  outDir: "public",
  publicDir: "public-assets",
  vite: {
    resolve: {
      alias: {
        "@src": path.resolve(__dirname, "./src"),
      },
    },
  },
  integrations: [
    svelte(),
    compress({
      path: "./public",
    }),
    image(),
  ],
});
